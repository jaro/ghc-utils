let
  sources = import nix/sources.nix;
  nixpkgs = import sources.nixpkgs {};
in

with nixpkgs;
let
  misc-scripts =
    stdenv.mkDerivation {
      name = "misc-scripts";
      nativeBuildInputs = [ makeWrapper ];
      preferLocalBuild = true;
      buildCommand = ''
        mkdir -p $out/bin

        cp ${./debug-ghc} $out/bin/debug-ghc
        chmod ugo+rx $out/bin/debug-ghc
        substituteInPlace $out/bin/debug-ghc \
          --replace tempfile ${debianutils}/bin/tempfile \
          --replace 'PROG="gdb' 'PROG="${gdb.gdb}/bin/gdb'

        makeWrapper ${./parallel-rr.py} $out/bin/parallel-rr \
          --prefix PATH : ${gdb.rr}/bin

        makeWrapper ${./wrap_ghc.py} $out/bin/wrap-ghc \
          --prefix PATH : ${python3}/bin
        makeWrapper ${./add-upstream-remotes.py} $out/bin/add-upstream-remotes
        makeWrapper ${./review-submodules} $out/bin/review-submodules
        makeWrapper ${./split-core2core.py} $out/bin/split-core2core
        makeWrapper ${./eventlog-sort.sh} $out/bin/eventlog-sort \
          --prefix PATH : ${haskellPackages.ghc-events}/bin:${gawk}/bin
        makeWrapper ${./run-until-crash} $out/bin/run-until-crash \
          --prefix PATH : ${python3}/bin
        makeWrapper ${./make-ghc-tags.sh} $out/bin/make-ghc-tags \
          --prefix PATH : ${haskellPackages.fast-tags}/bin
        makeWrapper ${./diff-submodules.py} $out/bin/diff-submodules \
          --prefix PATH : ${python3}/bin
        makeWrapper ${./build-cabal.sh} $out/bin/build-cabal

        makeWrapper ${./rts_stats.py} $out/bin/rts-stats
        makeWrapper ${./ghc_perf.py} $out/bin/ghc-perf
        makeWrapper ${./perf-compare.py} $out/bin/ghc-perf-compare \
          --prefix PATH : ${python3.withPackages (pkgs: with pkgs; [ matplotlib pandas tabulate ])}/bin
        makeWrapper ${./sample_proc.py} $out/bin/sample-proc \
          --prefix PATH : ${python3.withPackages (pkgs: with pkgs; [ matplotlib pandas tabulate ])}/bin
        makeWrapper ${./ghc_timings.py} $out/bin/ghc-timings \
          --prefix PATH : ${python3.withPackages (pkgs: with pkgs; [ matplotlib pandas tabulate ])}/bin
      '';
    };
  gdb = import ./gdb;
  gitlab-utils = import ./gitlab-utils { inherit pkgs; };
  rel-eng = import ./rel-eng { inherit nixpkgs; };
  compare-ticks = haskellPackages.callCabal2nix "compare-ticks" ./compare-ticks {};
  filter-dot = haskellPackages.callCabal2nix "filter-dot" ./filter-dot {
    algebraic-graphs = haskell.lib.dontCheck (haskellPackages.callHackage "algebraic-graphs" "0.7" {});
  };
in
  symlinkJoin {
    name = "ghc-utils";
    preferLocalBuild = true;
    paths = [
      gdb.rr gdb.gdb gdb.run-ghc-gdb gdb.run-ghc-rr gdb.dot2svg gdb.zenWorkaround
      misc-scripts
      rel-eng
      gitlab-utils
      compare-ticks
      filter-dot
    ];
  }
