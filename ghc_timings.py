#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import sys
import typing
import pandas as pd
from matplotlib import pyplot as pl
from typing import Iterator

class Timing(typing.NamedTuple):
    phase: str
    module: str
    time: float
    allocd: float

def parse_timings_raw(s: str) -> Iterator[Timing]:
    """ Parse timings from -ddump-timings """
    for m in re.finditer(r'([\w \d/]+) \[([\w\d_\.]+)\]: alloc=(\d+) time=(\d+\.\d+)', s):
        phase = m.group(1)
        module = m.group(2)
        allocd = int(m.group(3))
        time = float(m.group(4))
        yield Timing(phase, module, time, allocd)

def parse_timings_raw_verbose(s: str) -> Iterator[Timing]:
    """ Parse timings from ghc -v """
    for m in re.finditer(r'!!! ([\w \d/]+) \[([\w\d_\.]+)\]: finished in (\d+\.\d+) milliseconds, allocated (\d+\.\d+) megabytes', s):
        phase = m.group(1)
        module = m.group(2)
        time = float(m.group(3))
        allocd = float(m.group(4))
        yield Timing(phase, module, time, allocd)

def parse_timings(s: str) -> pd.DataFrame:
    xs = list(parse_timings_raw(s))
    if xs == []:
        xs = list(parse_timings_raw_verbose(s))

    df = pd.DataFrame(xs, columns=['phase', 'module', 'time', 'allocd'])
    df['n'] = df.index
    df.sort_values(by=['module', 'n'], inplace=True)
    return df # df.drop(columns='n')

def main() -> None:
    s = sys.stdin.read()
    df = parse_timings(s)
    print(df.to_markdown())

    df2 = df.set_index(['module', 'n', 'phase'])['time'].unstack('module')
    df2.plot.bar(title='compile time (ms)', rot=30)
    pl.savefig('timings.svg')

    # Group by module
    df2 = df.groupby('module').sum()['time']
    df2.plot.bar(title='compile time (ms)', rot=30)
    pl.savefig('timings.by-module.svg')

    # Group by phase
    df2 = df.groupby('phase').sum()['time']
    df2.plot.bar(title='compile time (ms)', rot=30)
    pl.savefig('timings.by-phase.svg')

if __name__ == '__main__':
    main()
