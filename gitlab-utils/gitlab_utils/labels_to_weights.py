#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gitlab
import click
from typing import List

LABELS = {
    'P::highest': 8,
    'P::high': 6,
    'P::normal': 4,
    'P::low': 2,
}

@click.command()
@click.option('--all', is_flag=True)
@click.argument('ticket', type=int, nargs=-1)
def cli(all: bool, ticket: List[int]):
    gl = gitlab.Gitlab.from_config('haskell')
    proj = gl.projects.get(1)
    if all:
        assert len(ticket) == 0
        issues = proj.issues.list(lazy=True)
    else:
        issues = [proj.issues.get(i) for i in ticket]

    for issue in issues:
        print(issue.iid)
        weight = None
        for label,w in LABELS.items():
            if label in issue.labels:
                weight = w

        if issue.weight != weight:
            issue.weight = weight
            issue.save()

if __name__ == '__main__':
    cli()

