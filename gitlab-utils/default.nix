{ pkgs }:

let
  python-gitlab = pkgs.python3Packages.python-gitlab.overrideAttrs (oldAttrs: {
    src = pkgs.fetchFromGitHub {
      owner = "python-gitlab";
      repo = "python-gitlab";
      rev = "e77224818e63e818c10a7fad69f90e16d618bdf7";
      sha256 = "sha256-m7igH7DMrmcWTkbfqTmj8a6C03mhE+An5iX8gg65tY0=";
    };
  });

  gitlab-utils = { buildPythonPackage, python-gitlab, click, dateutil }:
    buildPythonPackage {
      pname = "gitlab-utils";
      version = "0.0.1";
      src = ./.;
      propagatedBuildInputs = [ python-gitlab click dateutil ];
      preferLocalBuild = true;
      doCheck = false;
    };
in 
pkgs.python3Packages.callPackage gitlab-utils { inherit python-gitlab; }
