#!/usr/bin/env python

from distutils.core import setup

setup(
    name='gitlab-utils',
    author='Ben Gamari',
    author_email='ben@smart-cactus.org',
    packages=['gitlab_utils'],
    entry_points={
        'console_scripts': [
            'gitlab-labels=gitlab_utils.labels:main',
            'cleanup-docker=gitlab_utils.cleanup_docker:main',
            'subscribe_all=gitlab_utils.subscribe_all:main',
            'spam_util=gitlab_utils.spam_util:cli',
            'weights_to_labels=gitlab_utils.weights_to_labels:main',
            'labels_to_weights=gitlab_utils.labels_to_weights:cli',
            'gitlab-snippet=gitlab_utils.snippet:cli',
            'gitlab-cleanup-todos=gitlab_utils.cleanup_todos:main',
            'gitlab-issues-vs-time=gitlab_utils.issues_versus_time:main',
        ]
    }
)
