#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Deprecated: Use the in-tree version of this script in `.gitlab/upload_ghc_libs.py`
"""

print("Deprecated: Use the in-tree version of this script in `.gitlab/upload_ghc_libs.py`")
