-- | Generate a @dot@ graph from @dependencies.mk@.

import Data.Maybe
import Data.Bifunctor
import System.FilePath
import qualified Data.Text as T

main :: IO ()
main = do
    let mkfile = "_build/stage1/libraries/base/.dependencies.mk"

    rules <- parseMakefile <$> readFile mkfile
    let rules' =
            [ Rule targets' deps'
            | Rule targets deps <- rules
            , let targets' = mapMaybe (toSource . dropRoot) targets
            , not $ null targets'
            , let deps' = mapMaybe (toSource . dropRoot) deps
            , not $ null deps'
            ]
        dropRoot = makeRelative "libraries/base" . makeRelative "_build/stage1/libraries/base/build"

    putStrLn $ toDot rules'

toDot :: [Rule] -> String
toDot rules = unlines $
    [ "digraph {" ] ++
    [ edge (show target) (show dep) ++ ";"
    | rule <- rules
    , target <- targets rule
    , dep <- deps rule
    ] ++
    [ edge (show f) (show $ replaceExtension f ".hs") ++ " [bootEdge=True];"
    | rule <- rules
    , f <- targets rule
    , ".hs-boot" `isExtensionOf` f
    ] ++
    [ "}" ]
  where
    edge src tgt = unwords [src, "->", tgt]

data Rule = Rule { targets, deps :: [FilePath] }

parseMakefile :: String -> [Rule]
parseMakefile = mapMaybe f . T.lines . T.pack
  where
    f line
      | T.head line == '#' = Nothing
      | [targets0, deps0] <- T.splitOn (T.pack ":") line =
          Just $ Rule { targets = map T.unpack $ T.words targets0
                      , deps = map T.unpack $ T.words deps0
                      }

toSource :: FilePath -> Maybe FilePath
toSource fp
  | ".hs-boot" `isExtensionOf` fp = Just fp
  | ".hs" `isExtensionOf` fp = Just fp
  | ".hsc" `isExtensionOf` fp = Just $ replaceExtension fp ".hs"
  | ".hi-boot" `isExtensionOf` fp = Just $ replaceExtension fp ".hs-boot"
  | ".o-boot" `isExtensionOf` fp = Just $ replaceExtension fp ".hs-boot"
  | ".o" `isExtensionOf` fp = Just $ replaceExtension fp ".hs"
  | ".hi" `isExtensionOf` fp = Just $ replaceExtension fp ".hs"
  | otherwise = Nothing

