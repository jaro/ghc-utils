---
fontsize: 20pt
---

## STG Registers

STG       i386     amd64    aarch64     description
------    ------   ------   --------    ------------------
`Base`    `ebx`    `r13`    `x19`       Base register (`StgRegTable*`)
`Sp`      `ebp`    `rbp`    `x20`       Stack pointer
`SpLim`            `r15`    `x28`       Stack limit
`Hp`      `edi`    `r12`    `x21`       Heap pointer
`R1`      `esi`    `rbx`    `x22`       Current closure, return value
`R2`               `r14`    `x23`       arg1
`R3`               `rsi`    `x24`       arg2
`R4`               `rdi`    `x25`       arg3
`R5`               `r8`     `x26`       arg4
`R6`               `r9`     `x27`       arg5

Other arguments on stack

## SysV AMD64 Registers

C         amd64             Preserved across function calls
-----     ---------------   -------------------------------
return    `rax`             No
arg1      `rdi`             No
arg2      `rsi`             No
arg3      `rdx`             No
arg4      `rcx`             No
arg5      `r8`              No
arg6      `r9`              No
-         `r10` - `r11`     No
-         `r12` - `r15`     Yes
stack     `rsp`             Yes
frame     `rbp`             Yes
-         `rbx`             Yes
-         `xmm*`            No

\newpage

## Windows AMD64 Registers

C         amd64             Preserved across function calls
-----     ---------------   -------------------------------
return    `rax`             No
arg1      `rcx`             No
arg2      `rdx`             No
arg3      `r8`              No
arg4      `r9`              No
-         `r10`             No
-         `r11`             No
-         `r12` - `r15`     Yes
-         `rdi`             Yes
-         `rsi`             Yes
stack     `rsp`             Yes
frame     `rbp`             Yes
-         `rbx`             Yes
-         `r12` - `r14`     Yes
-         `xmm*`            No

\newpage

## Assembler syntax

Syntax      Order            Example
-------     -----------      ------------
C                            `$eax += *$ebx;`
AT&T        `op src, dest`   `add (%ebx), %eax`
Intel       `op dest, src`   `add eax, [ebx]`

## x86 flags and jumps

Flag      Name            Description
----      --------------  -------------------------------------------------------------
``CF``    carry flag      Set on high-order bit carry or borrow
``PF``    parity flag     Set if low-order eight bits contain an even number of ones
``ZF``    zero flag       Set if result is zero
``SF``    sign flag       Set if high-order bit of result is one
``OF``    overflow flag   Set if result overflows or underflows

```
TEST a, b ==> 
    CF  :=  0
    OF  :=  0
    SF  :=  (a & b) >> (XSIZE-1)
    ZF  :=  (a & b) == 0
    PF  :=  CountOnes(a & b & 0xff) is even

TEST a, a ==>
    ZF  :=  a == 0
    SF  :=  a < 0
```


Instruction       Description                     Flags
-----------       -----------------------------   ----------
`JO`              Jump if overflow                `OF = 1`
`JNO`             Jump if not overflow            `OF = 0`
`JS`              Jump if sign                    `SF = 1`
`JNS`             Jump if not sign                `SF = 0`
`JE`              Jump if equal                   `ZF = 1`
`JZ`              Jump if zero                    
`JNE`             Jump if not equal               `ZF = 0`
`JNZ`             Jump if not zero                
`JB`              Jump if below                   `CF = 1`
`JNAE`            Jump if not above or equal     
`JC`              Jump if carry
o
                  **unsigned comparison**
`JNB`             Jump if not below               `CF = 0`
`JAE`             Jump if above or equal      
`JNC`             Jump if not carry
`JBE`             Jump if below or equal          `CF=1 || ZF=1`
`JNA`             Jump if not above               
`JA`              Jump if above                   `CF=0 && ZF=0`
`JNBE`            Jump if not below or equal
o
                  **signed comparison**
`JL`              Jump if less                    `SF != OF`
`JNGE`            Jump if not greater or equal
`JGE`             Jump if greater or equal        `SF = OF`
`JNL`             Jump if not less                
`JLE`             Jump if less or equal           `ZF = 1 || SF != OF`
`JNG`             Jump if not greater             
`JG`              Jump if greater                 `ZF=0 && SF = OF`
`JNLE`            Jump if not less or equal   

\newpage

## Core Transformations

### Case-of-case
```haskell
case (case x of { K1 -> E1; K2 -> E2 }) of
  E1 -> F1
  E2 -> F2

                  =>

case x of
  K1 -> case E1 of { E1 -> F1; E2 -> F2 }
  K2 -> case E2 of { E1 -> F1; E2 -> F2 }
```

### Case of known constructor
```haskell
case K of K -> E

       =>

E
```

### Binder swap
```haskell
case x of K v1 v2 ... -> E

          =>

case x of x' { K v1 v2 ... ->
    let x = x' in E
```

### Worker-wrapper
