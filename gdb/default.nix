let
  sources = import ../nix/sources.nix;
  nixpkgs = import sources.nixpkgs {};
in

with nixpkgs; rec {
  ghc-gdb = python3Packages.buildPythonPackage {
    name = "ghc-gdb";
    src = ./.;
    preferLocalBuild = true;
    checkInputs = [ mypy ];
    checkPhase = ''
      mypy --ignore-missing-imports --exclude=build .
    '';
  };

  run-ghc-gdb = writeScriptBin "ghc-gdb" ''
    #!${pkgs.bash}/bin/bash
    set -eu -o pipefail

    ${gdb}/bin/gdb -x ${gdbinit}/gdbinit "$@"
  '';

  run-ghc-rr = writeScriptBin "ghc-rr" ''
    #!${pkgs.bash}/bin/bash
    set -eu -o pipefail

    args="$@"
    if [[ "$1" == "replay" ]]; then
      args="$args --debugger ${gdb}/bin/gdb -x ${gdbinit}/gdbinit"
    fi
    ${rr}/bin/rr $args
  '';

  libipt = stdenv.mkDerivation {
    name = "libipt";
    nativeBuildInputs = [ cmake ];
    src = sources.processor-trace;
  };

  gdb-walkers = sources.gdb-walkers;

  gdb = (nixpkgs.gdb.override {
    python3 = python3;
  }).overrideAttrs (oldAttrs: {
    buildInputs = oldAttrs.buildInputs ++ [ libipt ];
    configureFlags = oldAttrs.configureFlags ++ [ "--disable-werror" ];
    nativeBuildInputs = oldAttrs.nativeBuildInputs ++ [ flex bison file ];
    src = sources.binutils-gdb;
  });

  rr = nixpkgs.rr.overrideAttrs (oldAttrs: rec {
    src = sources.rr;
  });

  zenWorkaround = nixpkgs.runCommand "zen-workaround"
    { nativeBuildInputs = [ nixpkgs.python3 nixpkgs.makeWrapper ]; }
    ''
      mkdir -p $out/bin
      makeWrapper ${rr.src}/scripts/zen_workaround.py $out/bin/zen_workaround.py
    '';

  pythonEnv = python3.withPackages (_: [ ghc-gdb ]);

  env = symlinkJoin {
    name = "gdb-with-ghc-gdb";
    paths = [
      gdb pythonEnv gdbinit rr dot2svg
      run-ghc-gdb run-ghc-rr zenWorkaround
    ];
  };

  # useful to render `ghc closure-deps` output
  dot2svg = writeScriptBin "dot2svg" ''
    #!${pkgs.bash}/bin/bash
    set -eu -o pipefail

    if [[ $# == 0 ]]; then
      echo "Usage: $0 [dot file]"
      exit 1
    fi
    ${graphviz}/bin/dot -T svg -o $1.svg $1
  '';

  gdbinit = writeTextFile {
    name = "gdbinit";
    destination = "/gdbinit";
    text = let
      ghc-gdb-init = ''
        python sys.path = ["${pythonEnv}/lib/python${python3.pythonVersion}/site-packages"] + sys.path
        python
        if 'ghc_gdb' in globals():
            import importlib
            importlib.reload(ghc_gdb)
        else:
            try:
                import ghc_gdb
            except Exception as e:
                import textwrap
                print('Failed to load ghc_gdb:')
                print('  ', e)
                print("")
                print(textwrap.dedent("""
                  If the failure is due to a missing symbol or type try
                  running `import ghc_gdb` after running the inferior.
                  This will load debug information that is lazily
                  loaded.
                """))
        end

        echo The `ghc` command is now available.\n
      '';

      gdb-walkers-init = ''
        python sys.path = ["${gdb-walkers}"] + sys.path
        source ${gdb-walkers}/basic_config.py
        source ${gdb-walkers}/commands.py
        source ${gdb-walkers}/functions.py
      '';
    in lib.concatStringsSep "\n" [ ghc-gdb-init gdb-walkers-init ];
  };
}

