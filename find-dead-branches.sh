#!/usr/bin/env bash

#
# Identify merged wip/* branches.
#

set -o pipefail

check_branch() {
  branch="$1"
  subj="$(git show --quiet --format="%s" "$branch")"
  master_commit="$(git log origin/master --grep="$subj" --format=%H | head -n1)" || master_commit="unknown"
  if [ -z "$master_commit" ]; then master_commit="unmerged"; fi
  echo "$master_commit $branch"
}

find_branches() {
    export -f check_branch
    #for branch in $(git branch -r | grep origin/wip/); do check_branch $branch done
    git branch -r | grep origin/wip/ | sed 's/^ *//'| parallel "check_branch {}" > branches
    echo "$(wc -l branches) branches, $(grep ^unmerged branches | wc -l) dead"
}

retire_branch() {
    local name="$1"
    git push origin-push "$name:refs/archive/$name" ":$name"
}

retire_branches() {
    export -f retire_branch
    cat branches | grep -v "^unmerged" | parallel "retire_branch {}"
}

find_branches
#retire_branches
