#!/usr/bin/env python3

from pathlib import Path
import subprocess
import collections

def extract_all(archive: Path, out_dir: Path):
    archive = archive.resolve()
    members = subprocess.check_output(['ar', 't', archive], encoding='UTF-8').split('\n')
    out_dir.mkdir(parents=True)
    idxs = collections.defaultdict(lambda: 1)
    i = 0
    for member in members:
        member = member.strip()
        if member == '':
            continue

        n = idxs[member]
        subprocess.check_call(['ar', 'xN', str(n), archive, member], cwd=out_dir)
        out = out_dir / member
        final = out.with_stem(f'{i:03d}-{out.stem}')
        print(final)
        out.rename(final)

        idxs[member] += 1
        i += 1

def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('archive', type=Path)
    args = parser.parse_args()

    out_dir = Path(f'extracted-{args.archive.name}')
    extract_all(args.archive, out_dir)

if __name__ == '__main__':
    main()
