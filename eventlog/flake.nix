{
  description = "GHC Eventlog utilities";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  inputs.ghc-events = {
    url = "github:bgamari/ghc-events/wip/pretty-time";
    flake = false;
  };

  inputs.threadscope = {
    url = "github:haskell/ThreadScope";
    flake = false;
  };

  outputs = inputs@{ self, nixpkgs, flake-utils, ghc-events, threadscope }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system}; in
      rec {
        packages = rec {
          ghc-events = pkgs.haskell.packages.ghc925.callCabal2nix "ghc-events" inputs.ghc-events {};
          ThreadScope = pkgs.haskell.lib.doJailbreak (pkgs.haskell.packages.ghc925.callCabal2nix "ThreadScope" inputs.threadscope { inherit ghc-events; });
        };

        apps.default = apps.ghc-events;
        apps.ghc-events = {
          type = "app";
          program = "${packages.ghc-events}/bin/ghc-events";
        };

        apps.threadscope = apps.ThreadScope;
        apps.ThreadScope = {
          type = "app";
          program = "${packages.ThreadScope}/bin/threadscope";
        };
      });
}
