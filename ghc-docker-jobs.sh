#!/usr/bin/env bash
#
# Start a docker container which contains the same environment as a job defined in
# jobs.yaml. The job script can be executed by running `./run_script`.
#
# ./ghc-docker-jobs.sh <job-name>
#
#
# All support requests please SMS to Ben Gamari

set -e



usage() {
  echo "Usage: $0 JOB_NAME [COMMIT]"
  exit 1
}

if [[ -z "$JOB_NAME" ]]; then
  JOB_NAME="$1"
fi
if [[ -z "$JOB_NAME" ]]; then
  usage
fi
if [[ -z "$COMMIT" ]]; then
  COMMIT="$2"
fi
if [[ -z "$COMMIT" ]]; then
  COMMIT="master"
  echo "No commit given; using $COMMIT"
fi

jobs_yaml=$(curl "https://gitlab.haskell.org/ghc/ghc/-/raw/$COMMIT/.gitlab/jobs.yaml")
DOCKER_REV=$(curl "https://gitlab.haskell.org/ghc/ghc/-/raw/$COMMIT/.gitlab-ci.yml" | grep "  DOCKER_REV" | sed 's/  DOCKER_REV: \(.*\)/\1/')

echo "DOCKER_REV: $DOCKER_REV"

sel() {
  echo "$jobs_yaml" | tail -n +2 | jq -r ".\"$JOB_NAME\"$1"
}

if [[ -z "$BUILD_FLAVOUR" ]]; then
  BUILD_FLAVOUR="validate"
fi

IMAGE=$(sel ".image" | sed -e "s/\$DOCKER_REV/$DOCKER_REV/")
echo ${IMAGE}

VARIABLES=$(sel ".variables | to_entries | .[] | select(.value | . != null and . != \"\") | \"ENV \(.key) \(.value)\"")
echo "$VARIABLES"

RUN_SCRIPT=$(sel ".script | join(\" && \")")
echo $RUN_SCRIPT

tmp="$(mktemp -d)"
mkdir -p "$tmp"

cat >"$tmp/run_script" <<EOF
#! /usr/bin/env bash

set -e

$(sel ".script | join (\" \n\")")
EOF

cat "$tmp/run_script"


#C=$(docker container create --name="ghc-docker" $IMAGE)

cat >"$tmp/Dockerfile" <<EOF
FROM $IMAGE
RUN sudo sh -c 'echo "LC_ALL=en_US.UTF-8" >> /etc/environment'
RUN sudo sh -c 'echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen'
RUN sudo sh -c 'echo "LANG=en_US.UTF-8" > /etc/locale.conf'
RUN sudo locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8
RUN git config --global user.email "$(git config --global --get user.email)" && \
    git config --global user.name "$(git config --global --get user.name)"
# Update stretch repositories
RUN sudo sed -i -e 's/deb.debian.org/archive.debian.org/g' \
                -e 's|security.debian.org|archive.debian.org/|g' \
                -e '/stretch-updates/d' /etc/apt/sources.list
RUN if which apt-get; then \
      sudo apt-get update && sudo apt-get install -y vim htop tmux gdb strace; \
    elif which apk; then \
      sudo apk add vim htop tmux gdb strace; \
    elif which dnf; then \
      sudo dnf install -y vim htop tmux gdb strace; \
    elif which yum; then \
      sudo yum install -y vim htop tmux gdb strace; \
    fi
RUN git clone https://gitlab.haskell.org/bgamari/ghc-utils.git
RUN git clone https://gitlab.haskell.org/ghc/ghc.git && \
    cd ghc && \
    git checkout $COMMIT && \
    git submodule update --init
RUN cd ghc && \
    .gitlab/ci.sh setup && \
    .gitlab/ci.sh configure
WORKDIR /home/ghc/ghc
$VARIABLES
ENV CPUS 1
COPY "\$tmp/run_script" .
RUN sudo chmod +x run_script
CMD bash -c 'while true; do sleep 60; done;'
EOF

iidfile="$(mktemp)"
rm $iidfile
docker build --iidfile="$iidfile" "$tmp"
image="$(cat "$iidfile")"
rm -f "$iidfile"
#rm -R "$tmp"
echo "Development environment image is:"
echo "  $image"

cidfile="$(mktemp -u)"
docker create --cidfile="$cidfile" "$image"
container="$(cat "$cidfile")"
rm -f "$cidfile"
echo
echo "Development environment container is:"
echo "  $container"
echo
echo "To start another shell in container run:"
echo
echo "    docker exec -it $container bash -i"
echo

docker start "$container"
docker exec -it "$container" bash -i
echo
echo "To drop container run:"
echo
echo "    docker container rm --force --volumes $container"
echo
