#!/usr/bin/env python
# -*- coding: utf-8 -*-

import subprocess
from pathlib import Path
from typing import Dict

def list_submodules(repo: Path, rev: str) -> Dict[Path, str]:
    p = subprocess.run(['git', 'ls-tree', '-r', rev],
                       capture_output=True, encoding='UTF-8')
    result = {}
    for line in p.stdout.split('\n'):
        parts = line.split()
        if len(parts) == 0:
            continue
        elif parts[1] != 'commit':
            continue

        sha = parts[2]
        path = parts[3]
        result[path] = sha

    return result

def get_submodule_commit(repo: Path, submod: Path, commit: str) -> str:
    """
    Get the commit which the given submodule was sitting on in the given
    commit of the parent repository.
    """
    p = subprocess.run(['git', 'ls-tree', commit, submod],
                       capture_output=True, encoding='UTF-8')
    commit = p.stdout.read().split()[2]
    return commit

RED = "\u001b[31m"
GREEN = "\u001b[32m"
RESET = "\u001b[0m" 

def git_describe(repo: Path, rev: str) -> str:
    return subprocess.check_output(['git', 'describe', '--tags', '--always', rev], encoding='UTF-8', cwd=repo).strip()

def diff_submodules(repo: Path, rev1: str, rev2: str):
    submods1 = list_submodules(repo, rev1)
    submods2 = list_submodules(repo, rev2)

    print('Legend:')
    print(GREEN)
    print(f"  + Present in {rev2} but not {rev1}")
    print(f"  - Present in {rev2} and equivalent commit present in {rev1}")
    print(RED)
    print(f"  + Present in {rev1} but not {rev2}")
    print(f"  - Present in {rev1} and equivalent commit present in {rev2}")
    print(RESET)
    print()

    for path in submods2:
        print(f'{path}')
        try:
            describe1 = git_describe(repo / path, submods1[path]) if path in submods1 else "(not present)"
            describe2 = git_describe(repo / path, submods2[path])
            print(f'  {rev1}: {describe1}')
            print(f'  {rev2}: {describe2}')
            if path not in submods1:
                continue

            print(GREEN)
            subprocess.run(['git', '-C', path, 'cherry', '-v', submods1[path], submods2[path]])
            print(RED)
            subprocess.run(['git', '-C', path, 'cherry', '-v', submods2[path], submods1[path]])
            print(RESET)
        except Exception as e:
            print(f'{RED}error: {e}{RESET}\n')

def main() -> None:
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('rev1')
    parser.add_argument('rev2')
    args = parser.parse_args()
    diff_submodules(Path('.'), args.rev1, args.rev2)

if __name__ == '__main__':
    main()

