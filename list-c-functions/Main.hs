{- cabal:
    build-depends: base, bytestring, language-c
-}

import Data.List
import qualified Data.ByteString as BS
import Language.C
import Language.C.Data.Ident
import Language.C.Data.InputStream
import Language.C.Data.Position

main :: IO ()
main = do
    bs <- BS.getContents
    Right tcu <- pure $ parseC bs (position 0 "<stdin>" 0 0 Nothing)
    mapM_ putStrLn $ sort $ getFuncNames tcu

getFuncNames :: CTranslUnit -> [String]
getFuncNames (CTranslUnit externs _) =
    [ identName
    | CDeclExt cdecl <- externs
    , CDecl _ ((Just cdeclr, _, _) : _) _ <- pure $ cdecl
    , CDeclr (Just ident) _ _ _ _ <- pure cdeclr
    , Ident identName _ _ <- pure ident
    ]
