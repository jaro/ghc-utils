{-# LANGUAGE TypeFamilies #-}

module Main where

import qualified Data.Map.Strict as M
import qualified Data.Set as S
import Control.Monad.ST
import qualified Language.Dot as Dot
import Algebra.Graph.Labelled.AdjacencyMap (AdjacencyMap)
import qualified Algebra.Graph.Labelled.AdjacencyMap as AdjMap
import qualified Algebra.Graph.ToGraph as ToGraph
import System.Environment (getArgs)
import Data.Maybe (fromMaybe, fromJust)

main :: IO ()
main = do
    input:roots' <- getArgs
    dot <- readDot input
    let roots = S.fromList $ map (\n -> Dot.NodeId (Dot.StringId n) Nothing) roots'
    let gr = filterRevDeps roots $ dotToGraph dot
    writeFile ("filtered."++input) $ Dot.renderDot $ graphToDotDigraph gr

-- | Filter a graph to include only nodes from which any of the nodes in the
-- given set are reachable.
filterRevDeps :: S.Set Dot.NodeId -> DotGraph -> DotGraph
filterRevDeps ns gr =
    let ga = ToGraph.toAdjacencyMapTranspose (graph gr)
        vs :: S.Set Dot.NodeId
        vs = foldMap (S.fromList . ToGraph.reachable ga) ns
    in onGraph (AdjMap.induce (`S.member` vs)) gr

onGraph :: (AdjacencyMap (Maybe [Dot.Attribute]) Dot.NodeId -> AdjacencyMap (Maybe [Dot.Attribute]) Dot.NodeId)
        -> DotGraph -> DotGraph
onGraph f gr = gr { graph = graph'
                  , nodeAttrs = nodeAttrs gr `M.restrictKeys` AdjMap.vertexSet graph'
                  }
  where
    graph' = f (graph gr)

data DotGraph = DotGraph { graph :: AdjacencyMap (Maybe [Dot.Attribute]) Dot.NodeId
                         , nodeAttrs :: M.Map Dot.NodeId [Dot.Attribute]
                         }

dotToGraph :: Dot.Graph -> DotGraph
dotToGraph (Dot.Graph _ _ _ stmts) =
    DotGraph { graph = AdjMap.edges [ (Just e,a,b) | ((a,b),e) <- M.toList edges ]
             , nodeAttrs = nodes
             }
  where
    nodes :: M.Map Dot.NodeId [Dot.Attribute]
    nodes = M.unionsWith (<>)
        [ M.singleton nid attrs
        | Dot.NodeStatement nid attrs <- stmts
        ]
    edges :: M.Map (Dot.NodeId, Dot.NodeId) [Dot.Attribute]
    edges = M.unionsWith (<>)
        [ M.singleton (a,b) attrs
        | Dot.EdgeStatement es attrs <- stmts
        , let es' = [ n | Dot.ENodeId _ n <- es ]
        , (a,b) <- zip es' (tail es')
        ]

graphToDotDigraph :: DotGraph -> Dot.Graph
graphToDotDigraph =
    Dot.Graph Dot.UnstrictGraph Dot.DirectedGraph Nothing . graphToDotStmts

graphToDotStmts :: DotGraph -> [Dot.Statement]
graphToDotStmts gr =
    [ Dot.NodeStatement nid attrs
    | nid <- AdjMap.vertexList $ graph gr
    , let attrs = fromMaybe [] $ M.lookup nid (nodeAttrs gr)
    ] ++
    [ Dot.EdgeStatement ents attrs
    | (Just attrs, na, nb) <- AdjMap.edgeList (graph gr)
    , let ents = [ Dot.ENodeId Dot.NoEdge na, Dot.ENodeId Dot.DirectedEdge nb ]
    ]

readDot :: FilePath -> IO Dot.Graph
readDot fname = do
    result <- Dot.parseDot fname <$> readFile fname
    either (fail . show) return result

