{
  description = "GHC GC statistics";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = inputs@{ self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        hsPkgs = pkgs.haskell.packages.ghc925.override {
          overrides = self: super: {
            Chart = pkgs.haskell.lib.doJailbreak super.Chart;
            Chart-cairo = pkgs.haskell.lib.doJailbreak super.Chart-cairo;
          };
        };
      in
      rec {
        packages = rec {
          default = gc-stats;
          gc-stats = hsPkgs.callCabal2nix "gc-stats" ./. {
          };
        };

        apps.default = apps.gc-stats;
        apps.gc-stats = {
          type = "app";
          program = "${packages.gc-stats}/bin/gc-stats";
        };
        apps.plot-census = {
          type = "app";
          program = "${packages.gc-stats}/bin/plot-census";
        };
        apps.nonmoving-gc-stats = {
          type = "app";
          program = "${packages.gc-stats}/bin/nonmoving-gc-stats";
        };
        apps.dump-samples = {
          type = "app";
          program = "${packages.gc-stats}/bin/dump-samples";
        };
      });
}
