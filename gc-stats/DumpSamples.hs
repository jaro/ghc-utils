import System.Environment
import GHC.RTS.Events
import Samples
import Types

main :: IO ()
main = do
    label : inFile : _ <- getArgs
    Right elog <- readEventLogFromFile inFile
    let evs = sortEvents $ events $ dat elog
    let intervals = toIntervals label evs
    putStrLn $ unlines
        $ [ unwords [ show (intStart i)
                    , show (intEnd i)
                    , show (toSeconds $ intDuration i)
                    ]
          | i <- intervals
          ]

