{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DeriveGeneric #-}

module Types where

import GHC.Generics
import Data.Aeson (ToJSON(..), ToJSONKey)
import GHC.RTS.Events

newtype Cap = Cap Int
            deriving (Show, Ord, Eq, ToJSON, ToJSONKey)

data Interval = Interval { intStart, intEnd :: Timestamp }
              deriving (Show, Ord, Eq, Generic)
instance ToJSON Interval where
    toJSON (Interval a b) = toJSON [a,b]

intDuration :: Interval -> Timestamp
intDuration i = intEnd i - intStart i

toSeconds :: Timestamp -> Double
toSeconds = (/1e9) . realToFrac

