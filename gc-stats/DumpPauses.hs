{-# LANGUAGE RecordWildCards #-}

import System.Environment
import GcStats
import Types

main :: IO ()
main = do
    inFile : _ <- getArgs
    gcs <- readGcs inFile
    putStrLn $ unlines $ map fmtGc gcs

fmtGc :: Gc -> String
fmtGc gc@Gc{..} = unwords
    [ show generation
    , show $ toSeconds $ intDuration (gcInterval gc)
    ]

