import Data.Aeson (encode)
import Data.List (intercalate)
import qualified Data.ByteString.Lazy as BSL
import System.Environment
import GcStats
import NonMovingGcStats
import Types

main :: IO ()
main = do
    inFile : _ <- getArgs
    gcs <- readGcs inFile
    BSL.writeFile (inFile<>".json") $ encode gcs
    writeFile (inFile<>".tsv") $ unlines
        [ intercalate "\t" $ 
          [ show $ requestTime gc
          , show $ generation gc
          , show $ toSeconds $ gcEndTime gc - requestTime gc
          , show $ bytesCopied gc
          , "    "
          ] ++ map (show . toSeconds) (endIdleTimes gc)
        | gc <- gcs
        ]
