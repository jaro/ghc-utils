module TickyReport
    ( module TickyReport.Types
    , readTickyReport
    ) where

import qualified Data.Text.IO as T
import System.FilePath

import TickyReport.Types
import TickyReport.Eventlog
import TickyReport.Text

readTickyReport :: FilePath -> IO TickyReport
readTickyReport fp
  | takeExtension fp == "eventlog" = readEventlog fp
  | otherwise = parseReport <$> T.readFile fp
