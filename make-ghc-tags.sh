#!/usr/bin/env bash

set -e

if [ "$1" = "-e" ]; then
    emacs=1
    out=TAGS
    ctags=etags
else
    out=tags
    ctags=etags
fi

build_tags() {
    local args
    if [ -n "$emacs" ]; then args="-e"; fi
    d=$1
    if [ -d $d ]; then
        echo "generating tags for $d..."
        #hasktags -x --append -o $out $HASKTAGS_FLAGS $args $d
        fasttags -x --append -o $out $HASKTAGS_FLAGS $args $d
    fi
}

rm -f $out
build_tags compiler
#build_tags ghc
#build_tags libraries/base
#build_tags libraries/ghc-boot
#build_tags libraries/ghc-prim
#build_tags libraries/ghci
#build_tags libraries/hoopl
#build_tags iserv

#$ctags --append -o $out $(find -iname '*.h') $(find -iname '*.c')

